package ioc_demo;

public interface Coach {

	public String getDailyWorkout();
	
	public String getDailyFortune();
}
