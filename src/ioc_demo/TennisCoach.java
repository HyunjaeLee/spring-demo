package ioc_demo;

public class TennisCoach implements Coach {

	// inject fortuneservice
	private FortuneService fortuneService;
	
	public TennisCoach(FortuneService theFortuneService) {
		fortuneService = theFortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		
		return "Everyday Tennis!!";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}

}
