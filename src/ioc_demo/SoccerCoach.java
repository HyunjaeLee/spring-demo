package ioc_demo;

public class SoccerCoach implements Coach{
	
	private FortuneService fortuneService;
	
	private SoccerCoach(FortuneService theFortuneService){
		fortuneService = theFortuneService;
	}
	
	@Override
	public String getDailyWorkout() {
		return "Practice your amazing dribbling skills";
	}

	@Override
	public String getDailyFortune() {
		return fortuneService.getFortune();
	}
}
